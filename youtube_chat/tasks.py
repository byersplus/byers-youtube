import datetime
import logging
import os
from youtube_chat.google import GoogleError, get_current_livestreams, get_first_livestream, parse_youtube_datetime, to_youtube_date_string
from youtube_chat.utils import get_or_create, get_update_or_create
import requests

import celery

from youtube_chat import config
from youtube_chat.database import initialize, db_session
from youtube_chat.models import YouTubeChannel, YouTubeChatMessage
from youtube_chat.broker import YouTubeBroker

BROKER_URL = os.getenv("BROKER_URL", "amqp://guest:guest@localhost:5672/byers")
BACKEND_URL = os.getenv("BACKEND_URL", "rpc://guest:guest@localhost:5672/byers")
GOOGLE_CLIENT_ID = os.getenv("GOOGLE_CLIENT_ID")
GOOGLE_CLIENT_SECRET = os.getenv("GOOGLE_CLIENT_SECRET")
YOUTUBE_BASE_URL = "https://www.googleapis.com/youtube/v3/liveChat/messages"
LOGGER = logging.getLogger(__name__)

app = celery.Celery('youtube_chat.tasks', broker=BROKER_URL, backend=BACKEND_URL)
broker = YouTubeBroker(BROKER_URL)
broker.connect()

@app.task
def refresh_bot_token():
    settings = config.load_default_settings()
    LOGGER.info(f"Client-ID: {GOOGLE_CLIENT_ID}")
    bot_refresh_response = requests.post('https://accounts.google.com/o/oauth2/token', {
        'client_id': GOOGLE_CLIENT_ID,
        'client_secret': GOOGLE_CLIENT_SECRET,
        'refresh_token': settings['bot_auth']['refresh_token'],
        'grant_type': 'refresh_token'
    }).json()
    if 'access_token' not in bot_refresh_response:
        LOGGER.critical(f"Unable to refresh bot token, response: {repr(bot_refresh_response)}")
        return
    expiry_date = datetime.datetime.now() + datetime.timedelta(seconds=bot_refresh_response['expires_in']-5)
    settings['bot_auth'].update(bot_refresh_response)
    settings['bot_auth']['expires_at'] = to_youtube_date_string(expiry_date)
    config.save_default_settings(settings)
    refresh_bot_token.apply_async([], eta=expiry_date)

@app.task
def refresh_streamer_token():
    settings = config.load_default_settings()
    streamer_refresh_response = requests.post('https://accounts.google.com/o/oauth2/token', {
        'client_id': GOOGLE_CLIENT_ID,
        'client_secret': GOOGLE_CLIENT_SECRET,
        'refresh_token': settings['streamer_auth']['refresh_token'],
        'grant_type': 'refresh_token'
    }).json()
    if 'access_token' not in streamer_refresh_response:
        LOGGER.critical(f"Unable to refresh streamer token, response: {repr(streamer_refresh_response)}")
        return
    expiry_date = datetime.datetime.now() + datetime.timedelta(seconds=streamer_refresh_response['expires_in']-5)
    settings['streamer_auth'].update(streamer_refresh_response)
    settings['streamer_auth']['expires_at'] = to_youtube_date_string(expiry_date)
    config.save_default_settings(settings)
    refresh_streamer_token.apply_async([], eta=expiry_date)

@app.task
def fetch_youtube_chat(page_token):
    settings = config.load_default_settings()
    if 'stop_fetching' in settings and settings['stop_fetching'] == True:
        LOGGER.info("Stopping fetch task...")
        settings['stop_fetching'] = False
        config.save_default_settings(settings)
        return

    if 'current_livechat' not in settings:
        livestream = get_first_livestream(settings['streamer_auth']['access_token'])
        if not livestream:
            LOGGER.error("No livestreams available!")
            return
        settings['current_livechat'] = livestream.live_chat_id
        config.save_default_settings(settings)
    livechat_id = settings['current_livechat']
    token = settings['bot_auth']['access_token']
    parameters = {
        'liveChatId': livechat_id,
        'part': 'snippet,authorDetails'
    }
    if page_token:
        parameters['pageToken'] = page_token
    chat_response = requests.get(YOUTUBE_BASE_URL, parameters, headers={
        'Authorization': f'Bearer {token}'
    })
    LOGGER.info(f"Headers: {repr(dict(chat_response.headers))}")
    chat = chat_response.json()
    if 'error' in chat:
        error_obj = chat['error']
        errors = GoogleError.from_error_object(error_obj)
        if 'liveChatEnded' in errors or 'liveChatNotFound' in errors:
            livestreams = get_current_livestreams(settings['streamer_auth']['access_token'])
            first_stream = None
            for stream, success in livestreams:
                if not success:
                    stream_errors = stream
                    LOGGER.critical(f"An error occurred while streams from YouTube: {', '.join([err.message for _, err in stream_errors.items()])}")
                    # Unable to find a passable stream
                    return
                livestream, updated, created = stream
                if not first_stream:
                    first_stream = livestream
                if created:
                    broker.stream_created(livestream)
                if updated:
                    broker.stream_updated(livestream)
            settings['current_livechat'] = first_stream.live_chat_id
            config.save_default_settings(settings)
            fetch_youtube_chat.delay(None)
            return
        else:
            # Something we can't really figure out
            LOGGER.critical(f"An error occurred while getting chat messages from YouTube: {', '.join([err.message for _, err in errors.items()])}")
            return
    now = datetime.datetime.now()
    next_call = now + datetime.timedelta(milliseconds=chat['pollingIntervalMillis'])
    next_page_token = chat['nextPageToken']
    
    for message in chat['items']:
        author_channel, channel_updated, channel_created = get_update_or_create(db_session, YouTubeChannel, defaults={
            'channel_url': message['authorDetails']['channelUrl'],
            'display_name': message['authorDetails']['displayName'],
            'profile_image_url': message['authorDetails']['profileImageUrl'],
            'verified': message['authorDetails']['isVerified'],
            'moderator': message['authorDetails']['isChatModerator']
        }, channel_id=message['authorDetails']['channelId'])
        if channel_created:
            broker.new_channel_saved(author_channel)
        if channel_updated:
            broker.channel_updated(author_channel)
        if message['snippet']['type'] == 'textMessageEvent':
            message_object = YouTubeChatMessage(message_id=message['id'], live_chat_id=livechat_id, author_id=author_channel.channel_id, sent_at=parse_youtube_datetime(message['snippet']['publishedAt']), message=message['snippet']['textMessageDetails']['messageText'])
            db_session.merge(message_object)
            broker.message_received(message_object)
            print(f"[YouTube Chat] {'[M]' if author_channel.moderator else ''} {author_channel.display_name} >> {message_object.message}")
        elif message['snippet']['type'] == 'messageDeletedEvent':
            message_object: YouTubeChatMessage
            message_object = db_session.query(YouTubeChatMessage).filter_by(message_id=message['snippet']['messageDeletedDetails']['deletedMessageId']).one_or_none()
            message_object.deleted_by_id = author_channel.channel_id
            message_object.deleted = True
    db_session.commit()
    fetch_youtube_chat.apply_async([next_page_token], eta=next_call)