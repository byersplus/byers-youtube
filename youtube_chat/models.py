from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, DateTime, Text
from sqlalchemy.orm import relationship
from youtube_chat.database import Base

class YouTubeChannel(Base):
    __tablename__ = 'youtubechannel'
    channel_id = Column(String(100), primary_key=True)
    channel_url = Column(String(255))
    display_name = Column(String(255))
    profile_image_url = Column(String(255))
    verified = Column(Boolean())
    moderator = Column(Boolean())
    messages = relationship('YouTubeChatMessage', back_populates="author", foreign_keys="[YouTubeChatMessage.author_id]")

    def to_dict(self):
        return {
            'channel_id': self.channel_id,
            'channel_url': self.channel_url,
            'display_name': self.display_name,
            'profile_image_url': self.profile_image_url,
            'verified': self.verified,
            'moderator': self.moderator
        }

class YouTubeLiveStream(Base):
    __tablename__ = 'youtubelivestream'
    livestream_id = Column(String(255), primary_key=True)
    live_chat_id = Column(String(255))
    stream_date = Column(DateTime())
    stream_title = Column(Text())
    messages = relationship('YouTubeChatMessage', back_populates="stream")

    def to_dict(self):
        return {
            'livestream_id': self.livestream_id,
            'live_chat_id': self.live_chat_id,
            'stream_date': self.stream_date,
            'stream_title': self.stream_title
        }

class YouTubeChatMessage(Base):
    __tablename__ = 'youtubechatmessage'
    message_id = Column(String(255), primary_key=True)
    live_chat_id = Column(String(255))
    author_id = Column(String(100), ForeignKey('youtubechannel.channel_id'))
    author = relationship("YouTubeChannel", back_populates="messages", foreign_keys=[author_id])
    sent_at = Column(DateTime())
    message = Column(Text())
    deleted = Column(Boolean())
    deleted_by_id = Column(String(100), ForeignKey('youtubechannel.channel_id'), nullable=True)
    deleted_by = relationship('YouTubeChannel', foreign_keys=[deleted_by_id])
    livestream_id = Column(String(255), ForeignKey('youtubelivestream.livestream_id'))
    stream = relationship("YouTubeLiveStream", back_populates="messages", foreign_keys=[livestream_id])

    def to_dict(self):
        return {'message_id': self.message_id, 'live_chat_id': self.live_chat_id, 'author_id': self.author_id, 'sent_at': self.sent_at.__str__(), 'message': self.message, 'deleted_by_id': self.deleted_by_id}