from youtube_chat.models import YouTubeChannel, YouTubeChatMessage, YouTubeLiveStream
from kombu import Connection, Exchange, Queue

class YouTubeBroker:

    def __init__(self, connection_url) -> None:
        self.message_exchange = Exchange('youtube.messages', 'topic')
        self.channel_exchange = Exchange('youtube.channels', 'topic')
        self.streams_exchange = Exchange('youtube.streams', 'topic')
        self.connection = Connection(connection_url)
        self.producer = self.connection.Producer(serializer='json')
    
    def connect(self):
        self.connection.connect()
        self.channel = self.connection.channel()
        self.message_exchange = self.message_exchange.maybe_bind(self.channel)
        self.channel_exchange = self.channel_exchange.maybe_bind(self.channel)
        self.streams_exchange = self.streams_exchange.maybe_bind(self.channel)
        self.message_exchange.declare()
        self.channel_exchange.declare()
        self.streams_exchange.declare()
    
    def disconnect(self):
        self.connection.release()

    def message_received(self, chat_message: YouTubeChatMessage):
        self.producer.publish(chat_message.to_dict(), routing_key='youtube.messages.new_message', exchange=self.message_exchange)
    
    def new_channel_saved(self, channel: YouTubeChannel):
        self.producer.publish(channel.to_dict(), routing_key='youtube.channels.new_channel', exchange=self.channel_exchange)
    
    def channel_updated(self, channel: YouTubeChannel):
        self.producer.publish(channel.to_dict(), routing_key='youtube.channels.updated_channel', exchange=self.channel_exchange)
    
    def stream_created(self, stream: YouTubeLiveStream):
        self.producer.publish(stream.to_dict(), routing_key='youtube.streams.new_livestream', exchange=self.streams_exchange)
    
    def stream_updated(self, stream: YouTubeLiveStream):
        self.producer.publish(stream.to_dict(), routing_key='youtube.streams.updated_livestream', exchange=self.streams_exchange)