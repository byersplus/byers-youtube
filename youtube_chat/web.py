import os
from typing import *
from urllib.parse import urlencode
import datetime
from youtube_chat.google import get_livechat_id, parse_youtube_datetime, to_youtube_date_string

import requests
from fastapi import FastAPI, Request
from fastapi.responses import RedirectResponse

from youtube_chat import config as byers_config
from youtube_chat.database import initialize as initialize_db
from youtube_chat.tasks import refresh_streamer_token, refresh_bot_token, fetch_youtube_chat, broker

GOOGLE_OAUTH_AUTH = "https://accounts.google.com/o/oauth2/"
GOOGLE_CLIENT_ID = os.getenv("GOOGLE_CLIENT_ID")
GOOGLE_CLIENT_SECRET = os.getenv("GOOGLE_CLIENT_SECRET")

app = FastAPI()
settings = byers_config.load_default_settings()

@app.on_event("startup")
def startup_event():
    initialize_db(None)

@app.get('/oauth/getbottoken')
def get_bot_token(request: Request):
    parameters = {
        'client_id': GOOGLE_CLIENT_ID,
        'redirect_uri': f"{request.base_url}oauth/bot/oauth2callback",
        'scope': 'https://www.googleapis.com/auth/youtube',
        'response_type': 'code',
        'access_type': 'offline'
    }
    return RedirectResponse(f"{GOOGLE_OAUTH_AUTH}auth?{urlencode(parameters)}")

@app.get('/oauth/getstreamertoken')
def get_streamer_token(request: Request):
    parameters = {
        'client_id': GOOGLE_CLIENT_ID,
        'redirect_uri': f"{request.base_url}oauth/streamer/oauth2callback",
        'scope': 'https://www.googleapis.com/auth/youtube',
        'response_type': 'code',
        'access_type': 'offline'
    }
    return RedirectResponse(f"{GOOGLE_OAUTH_AUTH}auth?{urlencode(parameters)}")

@app.get('/oauth/bot/oauth2callback')
def bot_callback(code: str = None, request: Request = None):
    if not code:
        return {"error": "access_denied"}
    google_response = requests.post(f"{GOOGLE_OAUTH_AUTH}token", {
        'code': code,
        'client_id': GOOGLE_CLIENT_ID,
        'client_secret': GOOGLE_CLIENT_SECRET,
        'redirect_uri': f"{request.base_url}oauth/bot/oauth2callback",
        'grant_type': "authorization_code"
    })
    token_data = google_response.json()
    if 'access_token' not in token_data:
        return {"error": "no access token"}
    expiry_date = datetime.datetime.now() + datetime.timedelta(seconds=token_data['expires_in']-5)
    token_data['expires_at'] = to_youtube_date_string(expiry_date)
    settings['bot_auth'] = token_data
    byers_config.save_default_settings(settings)
    refresh_bot_token.apply_async(eta=expiry_date)
    return {"result": True}

@app.get('/oauth/streamer/oauth2callback')
def streamer_callback(code: str = None, request: Request = None):
    if not code:
        return {"error": "access_denied"}
    google_response = requests.post(f"{GOOGLE_OAUTH_AUTH}token", {
        'code': code,
        'client_id': GOOGLE_CLIENT_ID,
        'client_secret': GOOGLE_CLIENT_SECRET,
        'redirect_uri': f"{request.base_url}oauth/streamer/oauth2callback",
        'grant_type': "authorization_code"
    })
    token_data = google_response.json()
    if 'access_token' not in token_data:
        return {"error": "no access token"}
    expiry_date = datetime.datetime.now() + datetime.timedelta(seconds=token_data['expires_in']-5)
    token_data['expires_at'] = to_youtube_date_string(expiry_date)
    settings['streamer_auth'] = token_data
    byers_config.save_default_settings(settings)
    refresh_streamer_token.apply_async(eta=expiry_date)
    return {"result": True}

@app.get('/oauth/bot/refresh')
def refresh_bot_token_web():
    settings = byers_config.load_default_settings()
    refresh_bot_token.delay()

@app.get('/start')
def start_livechat(livechat_id: str = None):
    if livechat_id:
        settings['current_livechat'] = livechat_id
        byers_config.save_default_settings(settings)
    fetch_youtube_chat.delay(None)
    return {"result": True}

@app.get('/startvideo/{video_id}')
def start_livechat_by_video_id(video_id: str):
    livechat_id, success = get_livechat_id(settings['bot_auth']['access_token'], video_id)
    if not success:
        return livechat_id
    settings['current_livechat'] = livechat_id
    byers_config.save_default_settings(settings)
    fetch_youtube_chat.delay(None)
    return {"result": True}

@app.get('/stop')
def stop_livechat():
    settings['stop_fetching'] = True
    byers_config.save_default_settings(settings)
    return {"result": True}

@app.get('/debug/startlivechat')
def start_livechat(video_id: str):
    livechat_id, success = get_livechat_id(settings['bot_auth']['access_token'], video_id)
    if not success:
        return livechat_id
    fetch_youtube_chat.delay(livechat_id, None)
    return {"result": True}

@app.get('/debug/settings')
def view_settings(load: bool = False):
    if load:
        global settings
        settings = byers_config.load_default_settings()
    return settings