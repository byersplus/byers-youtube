"""
This is used for getting static settings for the application
"""
import datetime
import os
import os.path
import json
from typing import Any, Dict

DEFAULT_SETTINGS_PATH = os.getenv("DEFAULT_SETTINGS_PATH", "settings.json")

DEFAULT_SETTINGS = {
    'bot_auth': {},
    'streamer_auth': {},
    'client_id': "",
    'client_secret': ""
}

def load_default_settings():
    return load_settings_file(DEFAULT_SETTINGS_PATH)

def save_default_settings(settings):
    return save_settings_file(DEFAULT_SETTINGS_PATH, settings)

def load_settings_file(path) -> Dict[str, Any]:
    if not os.path.isfile(path):
        save_settings_file(path, DEFAULT_SETTINGS)
    data = {}
    with open(path, 'r') as fp:
        data = json.load(fp)
    return data

def save_settings_file(path, settings) -> None:
    with open(path, 'w') as fp:
        json.dump(settings, fp)