import datetime
from typing import Dict
from youtube_chat.utils import get_update_or_create
from youtube_chat.models import YouTubeLiveStream
from youtube_chat.database import db_session
import requests
import json

def parse_youtube_datetime(date: str, with_millis=True):
    if with_millis:
        try:
            return datetime.datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.%fZ')
        except:
            return parse_youtube_datetime(date, with_millis=False)
    else:
        return datetime.datetime.strptime(date, '%Y-%m-%dT%H:%M:%SZ')

def to_youtube_date_string(date: datetime.datetime, with_millis=True):
    if with_millis:
        return date.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
    else:
        return date.strftime('%Y-%m-%dT%H:%M:%SZ')

def get_livechat_id(token, video_id):
    response = requests.get('https://www.googleapis.com/youtube/v3/videos', {
        'part': 'liveStreamingDetails',
        'id': video_id
    }, headers={
        'Authorization': f'Bearer {token}'
    })
    videos_obj = response.json()
    if 'error' in videos_obj:
        errors = GoogleError.from_error_object(videos_obj["error"])
        errors = [err.message for _, err in errors.items()]
        return errors, False
    first_video = videos_obj['items'][0]
    return first_video['liveStreamingDetails']['activeLiveChatId'], True

def get_current_livestreams(streamer_token):
    response = requests.get('https://www.googleapis.com/youtube/v3/liveBroadcasts', {
        'part': 'snippet',
        'broadcastType': 'all',
        'mine': 'true'
    }, headers={
        'Authorization': f'Bearer {streamer_token}'
    })
    broadcasts = response.json()
    if 'error' in broadcasts:
        errors = GoogleError.from_error_object(broadcasts["error"])
        errors = [err.message for _, err in errors.items()]
        yield errors, False
    for broadcast in broadcasts['items']:
        if 'liveChatId' not in broadcast['snippet']:
            continue
        stream, updated, created = get_update_or_create(db_session, YouTubeLiveStream, {
            'live_chat_id': broadcast['snippet']['liveChatId'],
            'stream_date': parse_youtube_datetime(broadcast['snippet']['publishedAt'], False),
            'stream_title': broadcast['snippet']['title']
        }, livestream_id=broadcast['id'])
        yield (stream, updated, created), True

def get_first_livestream(streamer_token):
    for obj, success in get_current_livestreams(streamer_token):
        if not success:
            return None
        return obj[0]
    return None

class GoogleError:

    message: str
    domain: str
    reason: str
    code: int

    @classmethod
    def from_error_object(cls, error_object):
        errors = {}
        for error in error_object['errors']:
            err = GoogleError()
            err.message = error["message"]
            err.domain = error["domain"]
            err.reason = error["reason"]
            err.code = error_object["code"]
            errors[err.reason] = err
        return errors