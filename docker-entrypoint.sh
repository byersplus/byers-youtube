#!/bin/sh
set -e

. /venv/bin/activate

echo "Waiting for database and broker"
while ! nc -z $DATABASE_HOST $DATABASE_PORT; do
    sleep 0.1
done
while ! nc -z $BROKER_HOST $BROKER_PORT; do
    sleep 0.1
done
echo "Database and broker are up"

exec uvicorn youtube_chat.web:app --host 0.0.0.0